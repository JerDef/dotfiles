syntax on

set exrc

" tell nvim to always display cursor as a block, even in Insert-mode
set guicursor=

" after searching for pattern, do not highlight last searched pattern!
set nohlsearch

set hidden

" no sounds in case of errors!
set noerrorbells

" do not wrap if text length is longer than page length
set nowrap

" display line numbers + makes jumping to lines much easier
set number relativenumber

" settings for tabs + indentation
set shiftwidth=4
set smartindent
set tabstop=4 softtabstop=4

set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile

set scrolloff=23

set noshowmode
set completeopt=menuone,noinsert,noselect

" display vertical line to prevent spaghetti code
set colorcolumn=100
set signcolumn=yes

" skeletons
autocmd BufNewFile *.c	0r ~/.config/nvim/skel/skel.c
autocmd BufNewFile *.sh	0r ~/.config/nvim/skel/skel.sh
autocmd BufNewFile *.html 0r ~/.config/nvim/skel/skel.html
